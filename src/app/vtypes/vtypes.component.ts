import { Component, OnInit } from '@angular/core';
import {Vtype} from '../vtype';
import {VTYPES} from '../mock-vtype';
import {VtypeService} from '../vtype.service';

@Component({
  selector: 'app-vtypes',
  templateUrl: './vtypes.component.html',
  styleUrls: ['./vtypes.component.css']
})
export class VtypesComponent implements OnInit {
  vtypes: Vtype[];
  selectedVtype: Vtype;

  constructor(private vtypeService: VtypeService) {
  }

  ngOnInit() {
    this.getVtypes();
  }

  onSelect(vtype: Vtype): void {
    this.selectedVtype = vtype;
  }
  getVtypes(): void {
    this.vtypeService.getVtypes().subscribe(vtypes => this.vtypes = vtypes);
  }
}
