import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtypesComponent } from './vtypes.component';

describe('VtypesComponent', () => {
  let component: VtypesComponent;
  let fixture: ComponentFixture<VtypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
