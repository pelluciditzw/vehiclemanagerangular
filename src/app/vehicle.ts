export class Vehicle {
  id: number;
  reg_tag: string;
  make: string;
  model: string;
  color: string;
  year: number;
  capacity: number;
}
