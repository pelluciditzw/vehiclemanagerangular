import {Vtype} from './vtype';


export const VTYPES: Vtype[] = [
  { id: 1, type: 'Car', capacity: 4, },
  { id: 2, type: 'Van', capacity: 17, },
  { id: 3, type: 'Bus', capacity: 22, },
];
