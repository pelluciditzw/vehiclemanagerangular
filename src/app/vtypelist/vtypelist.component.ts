import { Component, OnInit } from '@angular/core';
import {VtypeService} from '../vtype.service';
import {Vtype} from '../vtype';

@Component({
  selector: 'app-vtypelist',
  templateUrl: './vtypelist.component.html',
  styleUrls: ['./vtypelist.component.css']
})
export class VtypelistComponent implements OnInit {
  vtypes: Vtype[];
  selectedVtype: Vtype;

  constructor(private vtypeService: VtypeService) {
  }

  ngOnInit() {
    this.getVtypes();
  }

  onSelect(vtype: Vtype): void {
    this.selectedVtype = vtype;
  }
  getVtypes(): void {
    this.vtypeService.getVtypes().subscribe(vtypes => this.vtypes = vtypes);
  }
}
