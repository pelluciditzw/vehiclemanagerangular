import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtypelistComponent } from './vtypelist.component';

describe('VtypelistComponent', () => {
  let component: VtypelistComponent;
  let fixture: ComponentFixture<VtypelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtypelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtypelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
