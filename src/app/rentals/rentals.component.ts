import { Component, OnInit } from '@angular/core';
import {Rental} from '../rental';
import {RENTALS} from '../mock-rentals';
import {RentalService} from '../rental.service';

@Component({
  selector: 'app-rentals',
  templateUrl: './rentals.component.html',
  styleUrls: ['./rentals.component.css']
})
export class RentalsComponent implements OnInit {
  rentals: Rental[];
  selectedRental: Rental;

  constructor(private rentalService: RentalService) {
  }

  ngOnInit() {
    this.getRentals();
  }

  onSelect(rental: Rental): void {
    this.selectedRental = rental;
  }
  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }
}
