import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { MessageService } from './message.service';
import { Vtype } from './vtype';
import { VTYPES } from './mock-vtype';

@Injectable()
export class VtypeService {

  constructor(private messageService: MessageService) { }

  getVtypes(): Observable<Vtype[]> {
    // Todo: send the message _after_ fetching the vtypes
    this.messageService.add('Vehicle Type Service: fetched vtypes');
    return of(VTYPES);
  }

  getVtype(id: number): Observable<Vtype> {
    // Todo: send the message _after_ fetching the vtype
    this.messageService.add(`Vehicle Type Service: fetched vtype id=${id}`);
    return of(VTYPES.find(vtype => vtype.id === id));
  }
}
