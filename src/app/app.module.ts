import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { VehicleDetailComponent }  from './vehicle-detail/vehicle-detail.component';
import { VehiclesComponent }      from './vehicles/vehicles.component';
import { VehicleService }          from './vehicle.service';
import { MessageService }       from './message.service';
import { MessagesComponent }    from './messages/messages.component';

import { AppRoutingModule }     from './app-routing.module';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import {UserService} from './user.service';
import { AddUserComponent } from './add-user/add-user.component';
import { VtypesComponent } from './vtypes/vtypes.component';
import { VtypeDetailComponent } from './vtype-detail/vtype-detail.component';
import { AddVtypeComponent } from './add-vtype/add-vtype.component';
import {VtypeService} from './vtype.service';
import { RentalsComponent } from './rentals/rentals.component';
import { RentalDetailComponent } from './rental-detail/rental-detail.component';
import { HeroService } from './hero.service';
import {RentalService} from './rental.service';
import { AddRentalComponent } from './add-rental/add-rental.component';
import { VehiclelistComponent } from './vehiclelist/vehiclelist.component';
import { UserlistComponent } from './userlist/userlist.component';
import { VtypelistComponent } from './vtypelist/vtypelist.component';
import { VtypelistDetailComponent } from './vtypelist-detail/vtypelist-detail.component';
import { UserlistDetailComponent } from './userlist-detail/userlist-detail.component';
import { VehiclelistDetailComponent } from './vehiclelist-detail/vehiclelist-detail.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    VehiclesComponent,
    VehicleDetailComponent,
    MessagesComponent,
    AddVehicleComponent,
    UsersComponent,
    UserDetailComponent,
    AddUserComponent,
    VtypesComponent,
    VtypeDetailComponent,
    AddVtypeComponent,
    RentalsComponent,
    RentalDetailComponent,
    AddRentalComponent,
    VehiclelistComponent,
    UserlistComponent,
    VtypelistComponent,
    VtypelistDetailComponent,
    UserlistDetailComponent,
    VehiclelistDetailComponent
  ],
  providers: [ VehicleService, MessageService, UserService, VtypeService, HeroService, RentalService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
