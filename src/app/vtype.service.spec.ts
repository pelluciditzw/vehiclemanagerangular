import { TestBed, inject } from '@angular/core/testing';

import { VtypeService } from './vtype.service';

describe('VtypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VtypeService]
    });
  });

  it('should be created', inject([VtypeService], (service: VtypeService) => {
    expect(service).toBeTruthy();
  }));
});
