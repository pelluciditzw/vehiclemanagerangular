import { Injectable } from '@angular/core';

import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';

import {MessageService} from './message.service';
import { Rental } from './rental';
import {RENTALS} from './mock-rentals';

@Injectable()
export class RentalService {

  constructor(private messageService: MessageService) { }

  getRentals(): Observable<Rental[]> {
    // Todo: send the message _after_ fetching the rentals
    this.messageService.add('RentalService: fetched rentals');
    return of(RENTALS);
  }

  getRental(id: number): Observable<Rental> {
    // Todo: send the message _after_ fetching the rental
    this.messageService.add(`RentalService: fetched rental id=${id}`);
    return of(RENTALS.find(rental => rental.id === id));
  }
}
