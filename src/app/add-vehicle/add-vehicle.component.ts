import { Component, OnInit } from '@angular/core';
import {Vehicle} from '../vehicle';
import { VEHICLES} from '../mock-vehicles';

@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.css'],
})
export class AddVehicleComponent implements OnInit {

  vehicles = ['Ferrari', 'Mercadies', 'Suzuki', 'Ford'];
  addVehicle(newVehicle: string) {
    if (newVehicle) {
      this.vehicles.push(newVehicle);
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
