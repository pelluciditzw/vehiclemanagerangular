import {Vehicle} from './vehicle';


export const VEHICLES: Vehicle[] = [
  { id: 1, reg_tag: 'Ba101', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 2, reg_tag: 'Mc404', make: 'Porche', model: '911', color: 'Silver', year: 1990, capacity: 4, },
  { id: 3, reg_tag: 'BJ817', make: 'Toyota', model: 'Hiace', color: 'Gold', year: 2018, capacity: 4, },
  { id: 4, reg_tag: 'BT999', make: 'Ferrari', model: 'Fast', color: 'Red', year: 2002, capacity: 4, },
  { id: 5, reg_tag: 'SZ871', make: 'Suzuki', model: '98T', color: 'Blue', year: 2000, capacity: 4, },
/*  { id: 6, reg_tag: 'SM892', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 7, reg_tag: 'BU893', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 8, reg_tag: 'Ja887', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 9, reg_tag: 'TM827', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 10, reg_tag: 'HU823', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 11, reg_tag: 'BM800', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 12, reg_tag: 'SZ873', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 15, reg_tag: 'SJ321', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 28, reg_tag: 'BS972', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },
  { id: 16, reg_tag: 'MU887', make: 'Suzuki', model: 'Vitara', color: 'Red', year: 2000, capacity: 4, },  */
];
