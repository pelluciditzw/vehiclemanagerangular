import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Vehicle } from './vehicle';
import { VEHICLES} from './mock-vehicles';
import { MessageService } from './message.service';

@Injectable()
export class VehicleService {

  constructor(private messageService: MessageService) { }

  getVehicles(): Observable<Vehicle[]> {
    // Todo: send the message _after_ fetching the vehicles
    this.messageService.add('VehicleService: fetched vehicles');
    return of(VEHICLES);
  }

  getVehicle(id: number): Observable<Vehicle> {
    // Todo: send the message _after_ fetching the vehicle
    this.messageService.add(`VehicleService: fetched vehicle id=${id}`);
    return of(VEHICLES.find(vehicle => vehicle.id === id));
  }
}
