import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclelistDetailComponent } from './vehiclelist-detail.component';

describe('VehiclelistDetailComponent', () => {
  let component: VehiclelistDetailComponent;
  let fixture: ComponentFixture<VehiclelistDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclelistDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclelistDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
