import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {VehicleService} from '../vehicle.service';
import {Vehicle} from '../vehicle';
import {Location} from '@angular/common';

@Component({
  selector: 'app-vehiclelist-detail',
  templateUrl: './vehiclelist-detail.component.html',
  styleUrls: ['./vehiclelist-detail.component.css']
})
export class VehiclelistDetailComponent implements OnInit {
  @Input() vehicle: Vehicle;

  constructor(
    private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getVehicle();
  }

  getVehicle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.vehicleService.getVehicle(id)
      .subscribe(vehicle => this.vehicle = vehicle);
  }

  goBack(): void {
    this.location.back();
  }
}
