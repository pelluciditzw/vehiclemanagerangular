import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { VehiclesComponent }      from './vehicles/vehicles.component';
import { VehicleDetailComponent }  from './vehicle-detail/vehicle-detail.component';
import {UsersComponent} from './users/users.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {VtypeDetailComponent} from './vtype-detail/vtype-detail.component';
import {VtypesComponent} from './vtypes/vtypes.component';
import {RentalsComponent} from './rentals/rentals.component';
import {RentalDetailComponent} from './rental-detail/rental-detail.component';
import {VehiclelistComponent} from './vehiclelist/vehiclelist.component';
import {VtypelistComponent} from './vtypelist/vtypelist.component';
import {UserlistComponent} from './userlist/userlist.component';
import {VehiclelistDetailComponent} from './vehiclelist-detail/vehiclelist-detail.component';
import {VtypelistDetailComponent} from './vtypelist-detail/vtypelist-detail.component';
import {UserlistDetailComponent} from './userlist-detail/userlist-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: VehicleDetailComponent },
  { path: 'vehicles', component: VehiclesComponent },
  { path: 'user/:id', component: UserDetailComponent},
  { path: 'users', component: UsersComponent},
  { path: 'vtype/:id', component: VtypeDetailComponent},
  { path: 'vtype', component: VtypesComponent},
  { path: 'rental/:id', component: RentalDetailComponent},
  { path: 'rental', component: RentalsComponent},
  { path: 'vlist/:id', component: VehiclelistDetailComponent},
  { path: 'vlist', component: VehiclelistComponent},
  { path: 'vtypelist/:id', component: VtypelistDetailComponent},
  { path: 'vtypelist', component: VtypelistComponent},
  { path: 'ulist/:id', component: UserlistDetailComponent},
  { path: 'ulist', component: UserlistComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
