import { Component, OnInit } from '@angular/core';
import { Rental} from '../rental';
import { RENTALS} from '../mock-rentals';

@Component({
  selector: 'app-add-rental',
  templateUrl: './add-rental.component.html',
  styleUrls: ['./add-rental.component.css']
})
export class AddRentalComponent implements OnInit {

  rentals = ['Rental1', 'Rental2', 'Rental3', 'Rental4'];
  addRental(newRental: string) {
    if (newRental) {
      this.rentals.push(newRental);
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
