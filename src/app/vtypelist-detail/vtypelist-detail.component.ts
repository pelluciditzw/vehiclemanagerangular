import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {VtypeService} from '../vtype.service';
import {Vtype} from '../vtype';
import {Location} from '@angular/common';

@Component({
  selector: 'app-vtypelist-detail',
  templateUrl: './vtypelist-detail.component.html',
  styleUrls: ['./vtypelist-detail.component.css']
})
export class VtypelistDetailComponent implements OnInit {
  @Input() vtype: Vtype;

  constructor(
    private route: ActivatedRoute,
    private vtypeService: VtypeService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getVtype();
  }

  getVtype(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.vtypeService.getVtype(id)
      .subscribe(vtype => this.vtype = vtype);
  }

  goBack(): void {
    this.location.back();
  }
}
