import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtypelistDetailComponent } from './vtypelist-detail.component';

describe('VtypelistDetailComponent', () => {
  let component: VtypelistDetailComponent;
  let fixture: ComponentFixture<VtypelistDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtypelistDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtypelistDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
