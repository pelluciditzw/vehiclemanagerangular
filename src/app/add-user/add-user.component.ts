import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { USERS } from '../mock-user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  users = ['Bob', 'Ryan', 'Sandra', 'J'];
  addUser(newUser: string) {
    if (newUser) {
      this.users.push(newUser);
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
