import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVtypeComponent } from './add-vtype.component';

describe('AddVtypeComponent', () => {
  let component: AddVtypeComponent;
  let fixture: ComponentFixture<AddVtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
