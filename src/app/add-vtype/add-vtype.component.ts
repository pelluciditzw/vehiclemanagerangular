import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-vtype',
  templateUrl: './add-vtype.component.html',
  styleUrls: ['./add-vtype.component.css']
})
export class AddVtypeComponent implements OnInit {
  vtypes = ['Minibus', 'Zr', 'Truck'];
  addVtype(newVtype: string) {
    if (newVtype) {
      this.vtypes.push(newVtype);
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
