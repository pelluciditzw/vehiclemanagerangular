import {Component, Input, OnInit} from '@angular/core';
import {RentalService} from '../rental.service';
import {Rental} from '../rental';
import {RENTALS} from '../mock-rentals';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-rental-detail',
  templateUrl: './rental-detail.component.html',
  styleUrls: ['./rental-detail.component.css']
})
export class RentalDetailComponent implements OnInit {
  @Input() rental: Rental;

  constructor(
    private route: ActivatedRoute,
    private Rentalservice: RentalService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getRental();
  }

  getRental(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.Rentalservice.getRental(id)
      .subscribe(rental => this.rental = rental);
  }

  goBack(): void {
    this.location.back();
  }
}
