import {Component, Input, OnInit} from '@angular/core';

import { Vehicle }         from '../vehicle';
import { VehicleService }  from '../vehicle.service';

@Component({
  selector: 'app-vehiclelist',
  templateUrl: './vehiclelist.component.html',
  styleUrls: ['./vehiclelist.component.css']
})
export class VehiclelistComponent implements OnInit {
  vehicles: Vehicle[];
  selectedVehicle: Vehicle;

  constructor(private vehicleService: VehicleService) {
  }

  ngOnInit() {
    this.getVehicles();
  }

  onSelect(vehicle: Vehicle): void {
    this.selectedVehicle = vehicle;
  }
  getVehicles(): void {
    this.vehicleService.getVehicles().subscribe(vehicles => this.vehicles = vehicles);
  }
}
