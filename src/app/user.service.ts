import { Injectable } from '@angular/core';

import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';

import {MessageService} from './message.service';
import {User} from './user';
import {USERS} from './mock-user';

@Injectable()
export class UserService {

  constructor(private messageService: MessageService) { }

  getUsers(): Observable<User[]> {
    // Todo: send the message _after_ fetching the users
    this.messageService.add('UserService: fetched users');
    return of(USERS);
  }

  getUser(id: number): Observable<User> {
    // Todo: send the message _after_ fetching the user
    this.messageService.add(`UserService: fetched user id=${id}`);
    return of(USERS.find(user => user.id === id));
  }
}
