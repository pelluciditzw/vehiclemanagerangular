import {Component, Input, OnInit} from '@angular/core';
import {Vtype} from '../vtype';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {VtypeService} from '../vtype.service';

@Component({
  selector: 'app-vtype-detail',
  templateUrl: './vtype-detail.component.html',
  styleUrls: ['./vtype-detail.component.css']
})
export class VtypeDetailComponent implements OnInit {
  @Input() vtype: Vtype;

  constructor(
    private route: ActivatedRoute,
    private vtypeService: VtypeService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getVtype();
  }

  getVtype(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.vtypeService.getVtype(id)
      .subscribe(vtype => this.vtype = vtype);
  }

  goBack(): void {
    this.location.back();
  }
}
