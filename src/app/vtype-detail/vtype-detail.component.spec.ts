import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtypeDetailComponent } from './vtype-detail.component';

describe('VtypeDetailComponent', () => {
  let component: VtypeDetailComponent;
  let fixture: ComponentFixture<VtypeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
